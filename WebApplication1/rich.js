﻿(function (compareSalary) {
    'use strict';

    var data = [
        {
            amount: "None",
            max: -1,
            breakdown:
                {
                    EasternCape: 2714744,
                    FreeState: 1059483,
                    Gauteng: 4905979,
                    KwazuluNatal: 4244857,
                    Limpopo: 2275939,
                    Mpumalanga: 1696448,
                    NorthWest: 1496398,
                    NorthernCape: 446759,
                    WesternCape: 2195544
                }
        },
        {
            amount: "R 1 - R 400",
            max: 400,
            breakdown:
                {
                    EasternCape: 1544713,
                    FreeState: 560373,
                    Gauteng: 1086248,
                    KwazuluNatal: 2183133,
                    Limpopo: 1419156,
                    Mpumalanga: 834668,
                    NorthWest: 639166,
                    NorthernCape: 211687,
                    WesternCape: 481481
                }
        },
        {
            amount: "R 401 - R 800",
            max: 800,
            breakdown:
                {
                    EasternCape: 254083,
                    FreeState: 122059,
                    Gauteng: 316919,
                    KwazuluNatal: 361853,
                    Limpopo: 184583,
                    Mpumalanga: 127293,
                    NorthWest: 119447,
                    NorthernCape: 39314,
                    WesternCape: 144605
                }
        },
        {
            amount: "R 801 - R 1 600",
            max: 1600,
            breakdown:
                {
                    EasternCape: 872341,
                    FreeState: 382726,
                    Gauteng: 982556,
                    KwazuluNatal: 1164757,
                    Limpopo: 701191,
                    Mpumalanga: 447828,
                    NorthWest: 453368,
                    NorthernCape: 181198,
                    WesternCape: 672423
                }
        },
        {
            amount: "R 1 601 - R 3 200",
            max: 3200,
            breakdown:
                {
                    EasternCape: 217491,
                    FreeState: 148062,
                    Gauteng: 1014530,
                    KwazuluNatal: 489256,
                    Limpopo: 184781,
                    Mpumalanga: 224564,
                    NorthWest: 187823,
                    NorthernCape: 61469,
                    WesternCape: 489616
                }
        },
        {
            amount: "R 3 201 - R 6 400",
            max: 6400,
            breakdown:
                {
                    EasternCape: 155713,
                    FreeState: 105321,
                    Gauteng: 808848,
                    KwazuluNatal: 330343,
                    Limpopo: 127968,
                    Mpumalanga: 156334,
                    NorthWest: 166898,
                    NorthernCape: 44516,
                    WesternCape: 352856
                }
        },
        {
            amount: "R 6 401 - R 12 800",
            max: 12800,
            breakdown:
                {
                    EasternCape: 156597,
                    FreeState: 88045,
                    Gauteng: 676157,
                    KwazuluNatal: 294013,
                    Limpopo: 118503,
                    Mpumalanga: 127846,
                    NorthWest: 107579,
                    NorthernCape: 40617,
                    WesternCape: 299703
                }
        },
        {
            amount: "R 12 801 - R 25 600",
            max: 25600,
            breakdown:
                {
                    EasternCape: 101326,
                    FreeState: 55762,
                    Gauteng: 553585,
                    KwazuluNatal: 195696,
                    Limpopo: 82277,
                    Mpumalanga: 88259,
                    NorthWest: 62947,
                    NorthernCape: 24971,
                    WesternCape: 223514
                }
        },
        {
            amount: "R 25 601 - R 51 200",
            max: 51200,
            breakdown:
                {
                    EasternCape: 2945,
                    FreeState: 16791,
                    Gauteng: 276805,
                    KwazuluNatal: 72935,
                    Limpopo: 21314,
                    Mpumalanga: 31699,
                    NorthWest: 20186,
                    NorthernCape: 7085,
                    WesternCape: 93134
                }
        },
        {
            amount: "R 51 201 - R 102 400",
            max: 102400,
            breakdown:
                {
                    EasternCape: 645,
                    FreeState: 4041,
                    Gauteng: 98065,
                    KwazuluNatal: 17138,
                    Limpopo: 4449,
                    Mpumalanga: 782,
                    NorthWest: 5136,
                    NorthernCape: 1688,
                    WesternCape: 28401
                }
        },
        {
            amount: "R 102 401 - R 204 800",
            max: 204800,
            breakdown:
                {
                    EasternCape: 3967,
                    FreeState: 228,
                    Gauteng: 3061,
                    KwazuluNatal: 8521,
                    Limpopo: 2769,
                    Mpumalanga: 319,
                    NorthWest: 2749,
                    NorthernCape: 984,
                    WesternCape: 9616
                }
        },
        {
            amount: "R 204 801 or more",
            max: 204801,
            breakdown:
                {
                    EasternCape: 3019,
                    FreeState: 1658,
                    Gauteng: 20575,
                    KwazuluNatal: 5783,
                    Limpopo: 217,
                    Mpumalanga: 2346,
                    NorthWest: 1926,
                    NorthernCape: 703,
                    WesternCape: 6277
                }
        },
    ];

    compareSalary.Compare = function (amount) {
        var betterThan = {
            EasternCape: 0,
            FreeState: 0,
            Gauteng: 0,
            KwazuluNatal: 0,
            Limpopo: 0,
            Mpumalanga: 0,
            NorthWest: 0,
            NorthernCape: 0,
            WesternCape: 0,
            Total: 0,
        };

        data.forEach(function (_) {
            if (amount > _.max) {
                betterThan.EasternCape += _.breakdown.EasternCape;
                betterThan.FreeState += _.breakdown.FreeState;
                betterThan.Gauteng += _.breakdown.Gauteng;
                betterThan.KwazuluNatal += _.breakdown.KwazuluNatal;
                betterThan.Limpopo += _.breakdown.Limpopo;
                betterThan.Mpumalanga += _.breakdown.Mpumalanga;
                betterThan.NorthernCape += _.breakdown.NorthernCape;
                betterThan.NorthWest += _.breakdown.NorthWest;
                betterThan.WesternCape += _.breakdown.WesternCape;
                betterThan.Total += _.breakdown.EasternCape + _.breakdown.FreeState +
                                    _.breakdown.Gauteng + _.breakdown.KwazuluNatal +
                                    _.breakdown.Limpopo + _.breakdown.Mpumalanga +
                                    _.breakdown.NorthernCape + _.breakdown.NorthWest +
                                    _.breakdown.WesternCape;
            }
        });

        return betterThan;
    }

}(window.compareSalary = window.compareSalary || {}));

$(document).ready(function () {
    $("#comparebutton").click(function () {
        var populationLessTop = 46824559;
        var population = 46867063

        var result = compareSalary.Compare($("#yours").val());

        var percent = (populationLessTop - result.Total) / population * 100;
        if (percent < 0) {
            percent = (result.Total - populationLessTop) / population * 100;
        }

        var content = "<p>You are better paid than <strong>" + format("### ###,##", result.Total) + "</strong> people in South Africa.</p>" +
            "<p>That puts you in the top <strong>" + format("### ###,###", percent) + "%.</strong></p><ul>" +
            "<li>In Eastern Cape there are " + format("### ###,##", result.EasternCape) + " people who earn less than you.</li>" +
            "<li>In Free State there are " + format("### ###,##", result.FreeState) + " people who earn less than you.</li>" +
            "<li>In Gauteng there are " + format("### ###,##", result.Gauteng) + " people who earn less than you.</li>" +
            "<li>In Kwazulu-Natal there are " + format("### ###,##", result.KwazuluNatal) + " people who earn less than you.</li>" +
            "<li>In Limpopo there are " + format("### ###,##", result.Limpopo) + " people who earn less than you.</li>" +
            "<li>In Mpumalanga there are " + format("### ###,##", result.Mpumalanga) + " people who earn less than you.</li>" +
            "<li>In Northern Cape there are " + format("### ###,##", result.NorthernCape) + " people who earn less than you.</li>" +
            "<li>In North West there are " + format("### ###,##", result.NorthWest) + " people who earn less than you.</li>" +
            "<li>In Western Cape there are " + format("### ###,##", result.WesternCape) + " people who earn less than you.</li></ul>";

        $("#result").html(content);
    });
});